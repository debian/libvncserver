Source: libvncserver
Section: libs
Priority: optional
Maintainer: Peter Spiess-Knafl <dev@spiessknafl.at>
Build-Depends: debhelper (>= 11),
               libgnutls28-dev,
               libjpeg-dev,
               pkg-config,
               libgcrypt20-dev,
               zlib1g-dev
Standards-Version: 4.2.1
Homepage: http://libvnc.github.io
Vcs-Git: https://salsa.debian.org/debian/libvncserver.git
Vcs-Browser: https://salsa.debian.org/debian/libvncserver

Package: libvncclient1
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Breaks: libvncserver0 (<< 0.9.9+dfsg-3)
Replaces: libvncserver0 (<< 0.9.9+dfsg-3)
Multi-Arch: same
Description: API to write one's own VNC server - client library
 LibVNCServer makes writing a VNC server (or more correctly, a program
 exporting a framebuffer via the Remote Frame Buffer protocol) easy. It hides
 the programmer from the tedious task of managing clients and compression.
 .
 This package provides the client library.

Package: libvncserver1
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Multi-Arch: same
Description: API to write one's own VNC server
 LibVNCServer makes writing a VNC server (or more correctly, a program
 exporting a framebuffer via the Remote Frame Buffer protocol) easy. It hides
 the programmer from the tedious task of managing clients and compression.
 .
 This package provides the server library.

Package: libvncserver-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
         libvncclient1 (= ${binary:Version}),
         libvncserver1 (= ${binary:Version}),
         libgnutls28-dev,
         libjpeg-dev,
         zlib1g-dev,
         libvncserver-config
Multi-Arch: same
Description: API to write one's own VNC server - development files
 LibVNCServer makes writing a VNC server (or more correctly, a program
 exporting a framebuffer via the Remote Frame Buffer protocol) easy. It hides
 the programmer from the tedious task of managing clients and compression.
 .
 This is the development package which contains headers and static libraries
 for libvncserver.
